
var jsonfile = require('jsonfile')
var cheerio = require('cheerio')
var request = require('request')
var fs = require('fs')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var express = require('express')
var app = express()
var server = require('http').Server(app)
var json2xls = require('json2xls')



app.use(cookieParser()) // read cookies (needed for auth)
app.use(bodyParser()) // get information from html forms
app.use(bodyParser.json()) // get JSON data
app.use(json2xls.middleware)

var reqs = request.defaults({
  'proxy': 'http://ebecda81129042ceac237c23807afc94:@proxy.crawlera.com:8010'
})




function scrape (req, res) {
  console.log(req.body.url)
  fetch(req.body.url, function (err, data) {
    if (!err) {
      res.end(JSON.stringify(data))
    } else {
      console.log(err)
      res.status(500).end()
    }
  })
}

function convertToXlsl (req, res) {
  jsonfile.readFile('jsonArray.json', function (err, jsonArray) {
    if (!err) {
      res.xls('data.xlsl', jsonArray)
    } else {
      res.status(500).end()
    }
  })
}

var fetch = function (url, cb) {
  var userAgent = ['Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Ubuntu/10.10 Chromium/8.0.552.237 Chrome/8.0.552.237 Safari/534.10', 'Opera/9.80 (Linux armv6l ; U; CE-HTML/1.0 NETTV/3.0.1;; en) Presto/2.6.33 Version/10.60', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12', 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko', 'Opera/9.80 (Windows NT 6.1; WOW64; Edition Campaign 21) Presto/2.12.388 Version/12.16']
  var ua = userAgent[Math.floor((Math.random() * userAgent.length) + 0)]

  var options = {
    url: url,
    pool: { maxSockets: 1000 },
    ca: fs.readFileSync('crt/crawlera-ca.crt'),
    requestCert: true,
    rejectUnauthorized: true,
    headers: {
      'User-Agent': ua,
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
      'Accept-Language': 'en-US,en;q=0.8',
      'Method': 'GET'
    }
  }

  reqs(options, function (err, response, body) {
    if (err) {
      console.log(err + ' request err')
      cb(err)
    } else {
      var $ = cheerio.load(body)
      var year = $('select[name=year] option:selected').text()
      var name = []
      var position = []
      var salary = []
      $('.datatable .team-name').each(function (i, elem) {
        name.push($(this).text())
      })

      $('.datatable .rank-position').each(function (i, elem) {
        position.push($(this).text())
      })

      $('.datatable .info').each(function (i, elem) {
        salary.push($(this).text())
      })

      jsonfile.readFile('jsonArray.json', function (err, jsonArray) {
        if (!err) {
          for (var i in name) {
            var player = {}
            player.name = name[i]
            player.salary = salary[i]
            player.position = position[i]
            player.year = year
            jsonArray.push(player)
          }

          jsonfile.writeFile('jsonArray.json', jsonArray, function (err) {
            if (!err) {
              cb(null, jsonArray)
            } else {

            }
          })
        }
      })
    }
  })
}


app.post('/', scrape)
app.get('/toXlsl', convertToXlsl)


server.listen(80, '0.0.0.0', function () {
  console.log('Listening on 80')
})
